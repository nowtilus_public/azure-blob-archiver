# Azure Blob Archiver

[![coverage report](https://gitlab.com/nowtilus_public/azure-blob-archiver/badges/master/coverage.svg)](https://gitlab.com/nowtilus_public/azure-blob-archiver/commits/master) [![pipeline status](https://gitlab.com/nowtilus_public/azure-blob-archiver/badges/master/pipeline.svg)](https://gitlab.com/nowtilus_public/azure-blob-archiver/commits/master) [![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

Archive your unused Azure blobs and save money.

## Introduction

To store files in Azure the blob-storage([b]inary-[l]arge-[ob]ject) can be a good option. But if you do not need to access this files on a regular bases you are probably paing more than you shoud. This tool helps you to move blobs in the archive storage, save money and make those files available again when you need them.

## Install

Prerequisites: `node.js v10+`

```bash
  npm i -g @nowtilus/azure-blob-archiver
```

## Usage

```bash
  STORAGE_ACCOUNT_KEY="<your-azure-storage-account-key>" azure-blob-archiver [options] <your-azure-storage-account-key>
```

Note the OS differences when setting environment variables:

- Linux/Mac: export STORAGE_ACCOUNT_KEY="<your-azure-storage-account-key>"
- Windows: SET STORAGE_ACCOUNT_KEY="<your-azure-storage-account-key>"
- PowerShell: \$env:STORAGE_ACCOUNT_KEY="<your-azure-storage-account-key>"

### Options

```bash
  --container, -c  Container name. Default: Load all containers in the storage account.
  --tier, -t  Target access tier. Defaults to 'Archive' - Possible: 'Hot', 'Cool', 'Archive', 'Check'
  --before, -b  Only change files created before this date. - example: 2019-10-01
```

### Examples

#### Check the current status without changes

This can be usefull if you restored your blobs to `Hot` storage and want to check if your changes got applied already.

```bash
  azure-blob-archiver --tier Check my-storage-account
```

#### Archive all blobs in a container before 2019

```bash
  azure-blob-archiver --tier Archive --before 2019-01-01 --container my-container my-storage-account
```

### Restore all blobs in a storage account from archive to Hot

Note: Changes will take several hours to be applied.

```bash
  azure-blob-archiver --tier Hot my-storage-account
```

## Report

At the end of a run the tool will output you the result.
Current: Shows how much blobs are currently in which access tier
Scheduled: Shows how much blobs have been scheduled to be moved to another tier

In the example below one blob was scheduled to be moved from Archive to Hot

```bash
REPORT

Duration: 4m 47.5s
┌───────────┬──────┬──────┬─────────┬──────┐
│           │ Hot  │ Cool │ Archive │ Sum  │
├───────────┼──────┼──────┼─────────┼──────┤
│ Current   │ 1331 │ 0    │ 1       │ 1332 │
├───────────┼──────┼──────┼─────────┼──────┤
│ Scheduled │ 1    │ 0    │ 0       │ 1    │
└───────────┴──────┴──────┴─────────┴──────┘
```
