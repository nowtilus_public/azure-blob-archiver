#!/usr/bin/env node
'use strict'

const meow = require('meow')
const {
  BlobServiceClient,
  StorageSharedKeyCredential
} = require('@azure/storage-blob')
const {
  changeAccessTierForContiner,
  listContainersInStorageAccount,
  loadConfig,
  printReport
} = require('./lib')

/**
 * Main function
 *
 * @param {string} account - storage account name
 * @param {object} options
 * @param {string} options.container - select a dedicated container
 * @param {string} options.tier - target tier the blobs schould move to
 * @param {string} options.before - run only on files created before this date
 */
async function main (account, options) {
  const config = loadConfig(options, process.env)
  const sharedKeyCredential = new StorageSharedKeyCredential(
    account,
    config.accountKey
  )
  const blobServiceClient = new BlobServiceClient(
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  )

  let report = {
    current: {
      hot: 0,
      cool: 0,
      archive: 0
    },
    scheduled: {
      hot: 0,
      cool: 0,
      archive: 0
    }
  }

  const start = new Date()
  try {
    if (config.selectedContainer) {
      const container = { name: config.selectedContainer }
      report = await changeAccessTierForContiner({
        config,
        blobServiceClient,
        sharedKeyCredential,
        account,
        container,
        report,
        logger: console.log
      })
    } else {
      const containers = await listContainersInStorageAccount({
        blobServiceClient
      })
      for await (const container of containers) {
        report = await changeAccessTierForContiner({
          config,
          blobServiceClient,
          sharedKeyCredential,
          account,
          container,
          report,
          logger: console.log
        })
      }
    }
  } catch (e) {
    console.error('An error occured: ' + e.message)
    process.exit(1)
  }

  const end = new Date()
  printReport({
    start,
    end,
    report,
    logger: console.log
  })
}

const cli = meow(
  `
    Usage
      $ export STORAGE_ACCOUNT_KEY="<your-azure-storage-account-key>"
      $ azure-blob-archiver [options] <your-azure-storage-account-key>
 
    Options
      --container, -c  Container name. Default: Load all containers in the storage account. 
      --tier, -t  Target access tier. Defaults to 'Archive' - Possible: 'Hot', 'Cool', 'Archive', 'Check'
      --before, -b  Only change files created before this date. - example: 2019-10-01

    Examples
      1. Check the current status without changes:
        $ azure-blob-archiver --tier Check my-storage-account

      2. Archive all blobs in a container before 2019:
        $ azure-blob-archiver --tier Archive --created-before 2019-01-01 --container my-container my-storage-account

      3. Restore all blobs in a storage account from archive to Hot:
        (Changes will take several hours to be applied)
        $ azure-blob-archiver --tier Hot my-storage-account
`,
  {
    flags: {
      container: {
        type: 'string',
        alias: 'c'
      },
      tier: {
        type: 'string',
        alias: 't',
        default: 'Archive'
      },
      before: {
        type: 'string',
        alias: 'b'
      }
    }
  }
)

main(cli.input[0], cli.flags)
