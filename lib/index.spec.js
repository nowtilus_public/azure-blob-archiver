const {
  changeAccessTierForContiner,
  changeAccessTierOfBlobs,
  listContainersInStorageAccount,
  loadConfig,
  printReport
} = require('.')

const blobServiceClient = {
  getContainerClient (containerName) {
    return {
      listBlobsFlat () {
        if (containerName === 'error-container') throw new Error('Cannot access container')
        const iterable = {}
        iterable[Symbol.asyncIterator] = async function * () {
          yield { name: 'blob1' }
          yield { name: 'blob2' }
          yield { name: 'blob3' }
          yield { name: 'blob4' }
        }
        return iterable
      }
    }
  },
  listContainers () {
    const iterable = {}
    iterable[Symbol.asyncIterator] = async function * () {
      yield { name: 'container1' }
      yield { name: 'container2' }
      yield { name: 'container3' }
    }
    return iterable
  }
}

const sharedKeyCredential = {}

function getEmptyReport () {
  return {
    current: {
      hot: 0,
      cool: 0,
      archive: 0
    },
    scheduled: {
      hot: 0,
      cool: 0,
      archive: 0
    }
  }
}

describe('Test Azure Blob Archiver', () => {
  describe('Load Config', () => {
    test('Load the script config successfully - with before', () => {
      expect(loadConfig({ tier: 'Archive', container: 'test-container', before: '2019-01-01' }, { STORAGE_ACCOUNT_KEY: '123' })).toEqual({
        accountKey: '123',
        createdBefore: new Date('2019-01-01T00:00:00.000Z'),
        selectedContainer: 'test-container',
        targetAccessTier: 'Archive'
      })
    })
    test('Load the script config successfully - without before', () => {
      expect(loadConfig({ tier: 'Archive', container: 'test-container' }, { STORAGE_ACCOUNT_KEY: '123' })).toEqual({
        accountKey: '123',
        createdBefore: null,
        selectedContainer: 'test-container',
        targetAccessTier: 'Archive'
      })
    })
    test('Throw an error on wrong data', () => {
      expect(() => loadConfig({ tier: 'Wrong' }, { STORAGE_ACCOUNT_KEY: '123' })).toThrowError(new Error("Tier not supported. Select one of: 'Hot', 'Cool', 'Archive', 'Check'"))
    })
    test('Throw an error on missing environment wariables', () => {
      expect(() => loadConfig({ tier: 'Archive' }, {})).toThrowError(new Error('No storage account key provided. Use environment variable: STORAGE_ACCOUNT_KEY'))
    })
  })

  describe('Print Report', () => {
    test('Printing out reports', () => {
      const logger = jest.fn()
      const report = {
        current: {
          hot: 1,
          cool: 2,
          archive: 3
        },
        scheduled: {
          hot: 4,
          cool: 5,
          archive: 6
        }
      }
      printReport({ start: new Date(), end: new Date(), logger, report })
      expect(logger.mock.calls.length).toBe(3)
      expect(logger.mock.calls[2][0].includes('Current')).toBe(true)
      expect(logger.mock.calls[2][0].includes('Scheduled')).toBe(true)
      // The sum of the scheduled entries
      expect(logger.mock.calls[2][0].includes('15')).toBe(true)
    })
  })

  describe('Changing the access tier for a container', () => {
    test('Changing the access tier from Hot to Archive', async () => {
      const report = await changeAccessTierForContiner({
        config: {
          accountKey: '123',
          createdBefore: null,
          selectedContainer: 'test-container',
          targetAccessTier: 'Archive'
        },
        blobServiceClient,
        sharedKeyCredential,
        account: 'test-account',
        container: { name: 'test-container' },
        report: getEmptyReport(),
        logger: () => {}
      })
      expect(report).toEqual({
        current: {
          archive: 1,
          cool: 0,
          hot: 3
        },
        scheduled: {
          archive: 3,
          cool: 0,
          hot: 0
        }
      })
    })

    test('Check the status of blobs access', async () => {
      const report = await changeAccessTierForContiner({
        config: {
          accountKey: '123',
          createdBefore: null,
          selectedContainer: 'test-container',
          targetAccessTier: 'Check'
        },
        blobServiceClient,
        sharedKeyCredential,
        account: 'test-account',
        container: { name: 'test-container' },
        report: getEmptyReport(),
        logger: () => {}
      })
      expect(report).toEqual({
        current: {
          archive: 1,
          cool: 0,
          hot: 3
        },
        scheduled: {
          archive: 0,
          cool: 0,
          hot: 0
        }
      })
    })

    test('Throw error when list blobs in container fails', async () => {
      await expect(changeAccessTierForContiner({
        config: {
          accountKey: '123',
          createdBefore: null,
          targetAccessTier: 'Check'
        },
        blobServiceClient,
        sharedKeyCredential,
        account: 'test-account',
        container: { name: 'error-container' },
        report: getEmptyReport(),
        logger: () => {}
      })).rejects.toEqual(new Error('Cannot change access tiers in the container: error-container - Cannot access container'))
    })
  })

  describe('List all containers in a storage account', () => {
    test('listing the containers', async () => {
      const containers = await listContainersInStorageAccount({ blobServiceClient })
      expect(typeof containers[Symbol.asyncIterator]).toBe('function')

      const items = []
      for await (const container of containers) {
        items.push(container)
      }

      expect(items).toEqual([{ name: 'container1' }, { name: 'container2' }, { name: 'container3' }])
    })
  })

  describe('Change access tiers of a blob', () => {
    test('Changing the access tier for blobs created before 2019-12-31', async () => {
      const blobs = blobServiceClient.getContainerClient().listBlobsFlat()
      const report = await changeAccessTierOfBlobs({
        config: {
          accountKey: '123',
          targetAccessTier: 'Archive',
          createdBefore: new Date('2019-01-01')
        },
        blobServiceClient,
        sharedKeyCredential,
        account: 'test-account',
        container: { name: 'test-container' },
        blobs,
        report: getEmptyReport(),
        logger: () => {}
      })
      expect(report.current.hot).toBe(3)
      expect(report.scheduled.archive).toBe(1)
    })

    test('Throw an error when blob properties cannot be loaded', async () => {
      const logger = jest.fn()
      const blobs = await blobServiceClient.getContainerClient().listBlobsFlat()
      await changeAccessTierOfBlobs({
        config: {
          accountKey: '123',
          createdBefore: null,
          targetAccessTier: 'Archive'
        },
        blobServiceClient,
        sharedKeyCredential,
        account: 'test-account',
        container: { name: 'error-blob' },
        blobs,
        report: getEmptyReport(),
        logger
      })
      expect(logger.mock.calls.length).toBe(4)
      expect(logger.mock.calls[3][0]).toBe('Issue occured for blob https://test-account.blob.core.windows.net/error-blob/blob4 - Cannot get properties for this blob')
    })
  })
})
