var Spinner = require('cli-spinner').Spinner
const { BlockBlobClient } = require('@azure/storage-blob')
const Table = require('cli-table')
const prettyMilliseconds = require('pretty-ms')

/**
 * Change the access tier for a list of blobs
 *
 * @param {object} options
 * @param {object} options.config - script configuration
 * @param {object} options.sharedKeyCredential - Azure blob service credentials
 * @param {object} options.account - storage account name
 * @param {object} options.container - container name
 * @param {object} options.blobs - async iterable of blobs
 */
async function changeAccessTierOfBlobs ({
  config,
  sharedKeyCredential,
  account,
  container,
  blobs,
  report,
  logger
}) {
  const spinner = new Spinner('processing.. %s')
  spinner.setSpinnerString('|/-\\')
  spinner.start()

  for await (const blob of blobs) {
    const blobUrl = `https://${account}.blob.core.windows.net/${container.name}/${blob.name}`
    try {
      const blockBlobClient = new BlockBlobClient(
        blobUrl,
        sharedKeyCredential
      )
      const originalBlob = await blockBlobClient.getProperties()
      report.current[originalBlob.accessTier.toLowerCase()]++
      if (config.targetAccessTier === 'Check') continue
      if (config.createdBefore) {
        if (originalBlob.createdOn > config.createdBefore) continue
      }
      if (originalBlob.accessTier !== config.targetAccessTier) {
        await blockBlobClient.setAccessTier(config.targetAccessTier)
        const changedBlob = await blockBlobClient.getProperties()
        logger(
          `Changed Access Tier of ${blobUrl} to ${config.targetAccessTier} - currently ${changedBlob.accessTier}`
        )
        report.scheduled[config.targetAccessTier.toLowerCase()]++
      }
    } catch (e) {
      logger(
        `Issue occured for blob ${blobUrl} - ${e.message}`
      )
    }
  }

  spinner.stop()
  return report
}

/**
 * Return an async iterable for all blobs in a container
 *
 * @param {object} options
 * @param {object} options.blobServiceClient - Azure blob service client
 * @param {object} options.container - container name
 * @returns {AsyncIterable} blobs
 */
async function listBlobsInContainer ({ blobServiceClient, container, logger }) {
  logger(`\nContainer: ${container.name}`)
  const containerClient = blobServiceClient.getContainerClient(container.name)
  return containerClient.listBlobsFlat()
}

/**
 * Change the access tier on all blobs inside a container
 *
 * @param {object} options
 * @param {object} options.blobServiceClient - Azure blob service client
 * @returns {AsyncIterable} containers
 */
async function listContainersInStorageAccount ({ blobServiceClient }) {
  return blobServiceClient.listContainers()
}

/**
 * Change the access tier on all blobs inside a container
 *
 * @param {object} options
 * @param {object} options.config - script configuration
 * @param {object} options.blobServiceClient - Azure blob service client
 * @param {object} options.sharedKeyCredential - Azure blob service credentials
 * @param {object} options.account - storage account name
 * @param {object} options.container - container name
 */
async function changeAccessTierForContiner ({
  config,
  blobServiceClient,
  sharedKeyCredential,
  account,
  container,
  report,
  logger
}) {
  try {
    const blobs = await listBlobsInContainer({ blobServiceClient, container, logger })
    report = await changeAccessTierOfBlobs({
      config,
      sharedKeyCredential,
      account,
      container,
      blobs,
      report,
      logger
    })
  } catch (e) {
    throw new Error('Cannot change access tiers in the container: ' +
      container.name +
      ' - ' +
      e.message
    )
  }
  return report
}

/**
 * Load the configuration for the script from environment variables and cli options
 *
 * @param {object} options
 * @param {string} options.container - select a dedicated container
 * @param {string} options.tier - target tier the blobs schould move to
 * @param {string} options.before - run only on files created before this date
 */
function loadConfig (options, env) {
  if (!env.STORAGE_ACCOUNT_KEY) {
    throw new Error('No storage account key provided. Use environment variable: STORAGE_ACCOUNT_KEY')
  }
  if (!['Hot', 'Cool', 'Archive', 'Check'].includes(options.tier)) {
    throw new Error("Tier not supported. Select one of: 'Hot', 'Cool', 'Archive', 'Check'")
  }
  return {
    selectedContainer: options.container,
    accountKey: env.STORAGE_ACCOUNT_KEY,
    targetAccessTier: options.tier,
    createdBefore: options.before ? new Date(options.before) : null
  }
}

/**
 * Print the statistics for the current run
 *
 * @param {Date} start - start date
 * @param {Date} end - end date
 */
function printReport ({ start, end, logger, report }) {
  logger('\n\nREPORT\n')
  logger(`Duration: ${prettyMilliseconds(end - start)}`)
  const table = new Table({ head: ['', 'Hot', 'Cool', 'Archive', 'Sum'] })
  table.push(
    {
      Current: [
        report.current.hot,
        report.current.cool,
        report.current.archive,
        report.current.hot + report.current.cool + report.current.archive
      ]
    },
    {
      Scheduled: [
        report.scheduled.hot,
        report.scheduled.cool,
        report.scheduled.archive,
        report.scheduled.hot + report.scheduled.cool + report.scheduled.archive
      ]
    }
  )
  logger(table.toString())
}

module.exports = {
  changeAccessTierForContiner,
  changeAccessTierOfBlobs,
  listBlobsInContainer,
  listContainersInStorageAccount,
  loadConfig,
  printReport
}
