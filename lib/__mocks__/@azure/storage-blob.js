class BlockBlobClient {
  constructor (blob, sharedKeyCredential) {
    this.blob = blob
    this.sharedKeyCredential = sharedKeyCredential
  }

  async getProperties () {
    if (this.blob.includes('error-blob')) throw new Error('Cannot get properties for this blob')

    let date = new Date('2020-01-01')
    if (this.blob.includes('blob1')) date = new Date('2018-12-31')

    let accessTier = 'Hot'
    if (this.blob.includes('blob4')) accessTier = 'Archive'

    return {
      accessTier: accessTier,
      createdOn: date
    }
  }

  async setAccessTier () {}
}

module.exports = { BlockBlobClient }
